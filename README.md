# Self Organizing Map and River Reach Prioritization
This repository hosts the source files for preprocessing RGA data, using that 
preprocessed data to run a Self Organizing Map, and finally to optimize 
prioritization of river reaches. This is specifically done with Vermont and 
its watersheds in mind.

## Preprocessing Data Inputs for SOM
The necessary inputs needed to build the features for the SOM are:

1. RGA Phase 1 and Phase 2 downloaded as an excel file
    * https://anrweb.vt.gov/DEC/SGA/projects/exports/combined.aspx
2. Digital Elevation Model (DEM)
3. Phase 2 Assessed Reaches as a Shapefile
4. Shapefile for burning stream layer (could use Phase 2 shapefile)

To update and run the preprocessing script:

1. Update __SOM-Feature-Builder.py__ with the proper file paths
2. With Python and proper libraries installed call the script from the command line

`>> python SOM-Feature-Builder.py`

Files associated with the preprocessing:

* SOM-Feature-Builder.py
* utils.py
* geospatialutils.py

## Run the Self Organizing Map
The SOM can be run by calling the graphical user interface script (GUI):

`>> python som_selector_gui.py`

Files associated with the SOM:

* somutils.py
    * Where the SOM algorithm and helper functions live
* som_selector_gui.py
    * A gui tool that runs the SOM based on user inputs
* som_selector.py
    * The core handling of running the SOM from the gui inputs provided
* SOM-Driver-Example.py
    * A simple example of how to make calls to run an SOM in a script

## Run the River Prioritization Model
The RPM uses a Multi-Objective Genetic Algorithm built using the DEAP Python 
libary. The RPM is run through a gui and takes in a trained SOM as an input.

The RPM can be run by invoking the gui script:

`>> python river_prioritization_gui.py`

Files associated with the SOM:

* river_prioritization_gui.py
   * The user interface for running the RPM
* river_prioritization.py
   * The core processing for the RMP, invoked by the gui
* optimizer.py
   * Where the MOGA and DEAP optimization code lives

## Requirements for Windows
* Python 3 (although Python 2.7.15 is probably OK)
* Numpy (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Scipy (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Cython (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* GDAL (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Matplotlib (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Pandas (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* PyQt4 (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Rtree (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Shapely (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Scikit-Learn (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Scikit-Image (https://www.lfd.uci.edu/~gohlke/pythonlibs/)
* Microsoft Visuall C++ Build Tools (https://www.microsoft.com/en-us/download/confirmation.aspx?id=48159)
    * Download and click through setup helper
* pygeoprocessing +
* deap +


The libraries with the (https://www.lfd.uci.edu/~gohlke/pythonlibs/) links can 
be downloaded from that site, making sure you select the correct download 
depending on your version of Python. These downloads are pre-compiled and 
made for easy Windows install. Once downloaded they can be installed from a 
command line like follows:

`>> pip install GDAl-2.3.2-cp37-cp37m-win_amd64.whl`
If you don't have __pip__ it will give you a message and tell you how to get it.

The libraries with the '+' can be installed via pip on the command line:

`>> pip install pygeoprocessing`
   